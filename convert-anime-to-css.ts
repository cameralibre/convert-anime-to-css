/* global tl anime */
// paste this script after the anime timeline in your SVG document (within the same `<script>`, so that it can access the timeline).
// Open the SVG in a browser and it will be converted to CSS animation.

// @ts-ignore - tl doesn't exist in the standalone ts/js file
const animeData = getAnimeJsData(tl) // change 'tl' to name of your timeline
const css = convertDataToCss(animeData)
appendCssToDocument(css)
deleteAllScripts() // (including this one)

function getAnimeJsData (timeline: Anime) {
  const easingLookup = createLookupTable()
  const timelineDuration = timeline.duration
  const timelineData: TimelineData = { }

  // Each section of the timeline has its own anim object with all its details
  timeline.children.forEach(anim => {
    // an animation has a single target element
    anim.animations.forEach(animation => {
      const currentAnimation = getTimelineData(animation, anim.timelineOffset, easingLookup)
      const target = getTargetSelector(animation.animatable.target)
      const targetKeyframes = updateTargetKeyframes(timelineData, target, currentAnimation)
      timelineData[target] = targetKeyframes
    })
  })

  return { timelineData, timelineDuration }
}

function convertDataToCss (animeData: KeyframesAndDuration) {
  const { timelineData, timelineDuration } = animeData
  const separatedAnimations = separateConflictingTransforms(timelineData)
  
  const targets = Object.keys(separatedAnimations)
  const cssReadyKeyframes: PercentageTimelineData = { }
  const defaultEasings: Record <string, string> = { } 
  
  targets.forEach(target => {
    const keyframesWithOneEasing = pickOneEasingPerKeyframe(separatedAnimations[target])  
    const loopSafeKeyframes = makeLoopSafe(keyframesWithOneEasing)
    const percentageKeyframes = convertToPercentages(loopSafeKeyframes, timelineDuration)
    cssReadyKeyframes[target] = removeRedundantKeyframes(percentageKeyframes)
    defaultEasings[target] = getMostCommonEasing(cssReadyKeyframes[target])
  })
  
  const timingDeclaration = combineTargetsAndDuration(targets, timelineDuration)
  const easingDeclaration = makeTargetListForEachEasing(defaultEasings)
  const keyframesDeclaration = formatKeyframesAsCss(cssReadyKeyframes, defaultEasings)
  const css = ` */${timingDeclaration}${easingDeclaration}${keyframesDeclaration}`
  
  return css
}

// getAnimeJsData component functions
function createLookupTable (): Record<string, string> {
  const lookupTable: Record<string, string> = {
    '0.2,0.7': 'linear', // linear
    '0.04894348370484647,0.5460095002604531': 'cubic-bezier(0.12, 0, 0.39, 0)', // easeInSine
    '0.30901699437494745,0.8910065241883679': 'cubic-bezier(0.61, 1, 0.88, 1)', // easeOutSine
    '0.09549150281252627,0.7938926261462365': 'cubic-bezier(0.37, 0, 0.63, 1)', // easeInOutSine
    '0.04000000000000001,0.48999999999999994': 'cubic-bezier(0.11, 0, 0.5, 0)', // easeInQuad
    '0.3599999999999999,0.9099999999999999': 'cubic-bezier(0.5, 1, 0.89, 1)', // easeOutQuad
    '0.08000000000000002,0.82': 'cubic-bezier(0.45, 0, 0.55, 1)',  // easeInOutQuad
    '0.008000000000000002,0.3429999999999999': 'cubic-bezier(0.32, 0, 0.67, 0)', //easeInCubic
    '0.4879999999999999,0.973': 'cubic-bezier(0.33, 1, 0.68, 1)', // easeOutCubic
    '0.03200000000000001,0.8919999999999999': 'cubic-bezier(0.65, 0, 0.35, 1)', // easeInOutCubic
    '0.0016000000000000007,0.24009999999999992': 'cubic-bezier(0.5, 0, 0.75, 0)', //easeInQuart
    '0.5903999999999998,0.9919': 'cubic-bezier(0.25, 1, 0.5, 1)', // easeOutQuart
    '0.012800000000000006,0.9351999999999999': 'cubic-bezier(0.76, 0, 0.24, 1)', // easeInOutQuart
    '0.0003200000000000002,0.16806999999999994': 'cubic-bezier(0.64, 0, 0.78, 0)', //easeInQuint
    '0.6723199999999998,0.99757': 'cubic-bezier(0.22, 1, 0.36, 1)', // easeOutQuint
    '0.005120000000000003,0.96112': 'cubic-bezier(0.83, 0, 0.17, 1)', // easeInOutQuint
    '0.00006400000000000004,0.11764899999999995': 'cubic-bezier(0.7, 0, 0.84, 0)', //easeInExpo
    '0.7378559999999998,0.999271': 'cubic-bezier(0.16, 1, 0.3, 1)', // easeOutExpo
    '0.002048000000000001,0.976672': 'cubic-bezier(0.87, 0, 0.13, 1)', // easeInOutExpo
    '0.020204102886728803,0.285857157145715': 'cubic-bezier(0.55, 0, 1, 0.45)', //easeInCirc
    '0.5999999999999999,0.9539392014169457': 'cubic-bezier(0, 0.55, 0.45, 1)', // easeOutCirc
    '0.041742430504416006,0.8999999999999999': 'cubic-bezier(0.85, 0, 0.15, 1)', // easeInOutCirc
    '-0.05600000000000001,0.04899999999999982': 'cubic-bezier(0.36, 0, 0.66, -0.56)', //easeInBack
    '0.7439999999999998,1.099': 'cubic-bezier(0.34, 1.56, 0.64, 1)', // easeOutBack
    '-0.064,1.036': 'cubic-bezier(0.68, -0.6, 0.32, 1.6)' // easeInOutBack
  }
  // check for any additional custom beziers in the document
  const scriptContent = Array.from(document.getElementsByTagName('script')).filter(script => script.textContent?.includes('anime.timeline')).map(el => el.textContent).join()
  const bezierRegex = /cubicBezier\((\s*-?\d?.?\d*,){3}(\s*-?\d?.?\d*)\)/g
  const beziersUsed = (scriptContent.match(bezierRegex) !== null)
    ? scriptContent.match(bezierRegex)
    : []
  if (beziersUsed && beziersUsed.length) {
    // add custom beziers to the lookup table
    const validBeziers = beziersUsed.filter(hasValidBezierParams)
    const customLookup = validBeziers.reduce((acc: Record<string, string>, bezier: string) => {
      // @ts-ignore - anime is not reachable from the standalone script.
      const easing = anime.easing(bezier, 1)
      const fingerprint = `${easing(0.2)},${easing(0.7)}`
      acc[fingerprint] = toKebabCase(bezier)
      return acc
    }, {})

    return Object.assign(lookupTable, customLookup)
  } else {
    console.log('no custom bezier easings detected')
    return lookupTable
  }
}

function hasValidBezierParams (bezier: string) {
  const numbers: number[] = bezier.replace('cubicBezier(', '').replace(')', '').split(',').map(parseFloat)
  return Boolean(numbers.every(num => typeof num === 'number') && numbers.length === 4)
}

function getTimelineData (animation: Animation, timelineOffset: number, easingLookup: Record<string, string>) {
  const animatedProperty = animation.property
  const currentAnimation: KeyframesForTarget = {}

  animation.tweens.forEach(tween => {
    const tweenStart = tween.start + tween.delay + timelineOffset
    const tweenEnd = tween.end - tween.endDelay + timelineOffset
    const { fromValue, toValue } = getFromAndToValues(tween, animatedProperty)
    const easing = identifyEasing(tween, easingLookup)
    const startKeyframe = writeToKeyframe({}, animatedProperty, fromValue, easing)
    const endKeyframe = writeToKeyframe({}, animatedProperty, toValue)

    // this tween's start timing will often be the previous tween's end timing
    // so we may need to add to the existing keyframe, rather than just using startKeyframe
    currentAnimation[tweenStart] = (tweenStart in currentAnimation)
      ? writeToKeyframe(currentAnimation[tweenStart], animatedProperty, fromValue, easing)
      : startKeyframe

    currentAnimation[tweenEnd] = (tweenEnd in currentAnimation)
      ? writeToKeyframe(currentAnimation[tweenEnd], animatedProperty, toValue)
      : endKeyframe

  })
  return currentAnimation
}

function writeToKeyframe (obj: KeyFrame | Record<string,never>, propName: string, value: string, easing?: string): KeyFrame {
  // function might be passed an empty object, which will need to be structured as a KeyFrame (with 'easing').
  const keyframe = (Object.keys(obj).length === 0) ? { easing: {}} : obj as KeyFrame
  // create transform/property/easing objects if they don't exist, and write to them.
  const key: 'transform'|'property' = isTransform(propName) ? 'transform' : 'property'
  let updatedKeyframe = addIfNoProp(keyframe, key)
  updatedKeyframe[key]![propName] = value
  if (easing && easing.length) {
    updatedKeyframe = addIfNoProp(updatedKeyframe, 'easing')
    updatedKeyframe.easing![propName] = easing
  }
  return updatedKeyframe
}

function addIfNoProp (object: KeyFrame, key: 'transform'|'property'|'easing') {
  if (!(key in object)) {
    object[key] = {} 
  }
  return object
}

function getFromAndToValues (tween: Tween, propName: string): Record<string, string> {
  // anime adds 'px' to strokeDashoffset values for some reason
  return (propName === 'strokeDashoffset')
    ? { fromValue: tween.from.numbers[0].toString(), toValue: tween.to.numbers[0].toString() }
    : { fromValue: tween.from.original, toValue: tween.to.original }
}

function identifyEasing (tween: Tween, lookupTable: Record<string, string>) {
  const fingerprint = [tween.easing(0.2), tween.easing(0.7)].join()
  
  if (fingerprint in lookupTable) {
    return lookupTable[fingerprint]
  } else {
    console.warn('Unknown easing detected! 🙈️ \n\n This script cannot convert steps easings (yet), or bounce, elastic or spring physics easings. Using linear easing as a placeholder:', tween)
    return `linear /* replaces custom easing: ${fingerprint} */`
  }
}

function isTransform (propName: string) {
  const validTransforms = ['translate', 'translateX', 'translateY', 'translateZ', 'rotate', 'rotateX', 'rotateY', 'rotateZ', 'scale', 'scaleX', 'scaleY', 'scaleZ', 'skew', 'skewX', 'skewY', 'perspective', 'matrix', 'matrix3d']
  return validTransforms.includes(propName)
}

function getTargetSelector (target: SVGElement) {
  if (target.id && target.id.length) {
    return `#${target.id}`
  }
  const classList: string[] = Array.from(target.classList)
  if (classList && classList.length) {
    const selector: string = `${target.tagName}.${classList.join('.')}`
    if (target.matches(selector) && document.querySelectorAll(selector).length === 1) {
      return selector
    }
  } 
  // if those options don't work, create a new id for the element
  const parentNode = target.parentNode as SVGElement
  const parent = (parentNode.id) ? parentNode.id : parentNode.tagName
  const uniqueIdString: string = Math.random().toString(36).slice(-5)
  let generatedId = `${parent}-${target.tagName}-${uniqueIdString}`
  console.warn('Missing unique selector for ' + target.tagName + ' element in #' + parent + '\n\n', `🎲️ Giving target an id of '${generatedId}'`)
  target.setAttribute('id', generatedId)
  
  return `#${generatedId}`
}

function updateTargetKeyframes(timelineData: TimelineData, target: string, currentAnimation: KeyframesForTarget): KeyframesForTarget {
  if (!(target in timelineData)) {
    // there's nothing to update!
    return currentAnimation
  } else {
    // go through each keyframe and add data
    const existingKeyframes = timelineData[target]
    const updatedKeyframes = timelineData[target]
    for (const timing in currentAnimation) {
      const existingKeyframe = existingKeyframes[timing]
      const thisKeyframe = currentAnimation[timing]
      if (!(timing in existingKeyframes)) {
        // if there's no existing keyframe at that timing, just copy it over
        updatedKeyframes[timing] = thisKeyframe
      } else {
        // there is an existing keyframe at that timing. Copy & update it.
        if (!(timing in updatedKeyframes)) {
          updatedKeyframes[timing] = existingKeyframe
        }
        // add animated properties & easings
        updatedKeyframes[timing] = getKeyframeProperties(thisKeyframe).reduce((acc: KeyFrame, prop: string) => {
          const propType = (isTransform(prop)) ? 'transform' : 'property'
          const value = thisKeyframe[propType]![prop]
          const easing = thisKeyframe.easing[prop]
          return writeToKeyframe(acc, prop, value, easing)
        }, updatedKeyframes[timing])
      }
    }
    return updatedKeyframes
  }
}

// convertDataToCss component functions
function pickOneEasingPerKeyframe (keyframesObject: KeyframesForTarget) {
  const converted: KeyframesForTarget = { }

  for (const keyframeTiming in keyframesObject) {
    const keyframe = keyframesObject[keyframeTiming]
    const { easing: easings, ...otherProperties } = keyframe
    
    const propertiesWithEasing = Object.keys(easings)
    const hasSameValueAsFirst = (value: string) => value === easings[propertiesWithEasing[0]]
    const allTheSameEasing = Object.values(easings).every(hasSameValueAsFirst)

    if (propertiesWithEasing.length && allTheSameEasing) {
      const easing = { 'animation-timing-function': easings[propertiesWithEasing[0]] }

      converted[keyframeTiming] = { easing, ...otherProperties }
    } else if (propertiesWithEasing.length && !allTheSameEasing) {
      
      const transformProperties = propertiesWithEasing.filter(isTransform)
      const nonTransformProperties = propertiesWithEasing.filter(propName => !isTransform(propName))

      const linearLast = (array: string[]) => {
        return [...array].sort((a, b) => {
          return (easings[a].startsWith('linear'))
            ? (easings[b].startsWith('linear')) ? 0 : 1
            : -1
        })
      }

      let orderedEasings: string[] = []
      
      if (transformProperties.length === propertiesWithEasing.length) {
        // only transforms need to be sorted. Choose non-linear easings over linear.
        orderedEasings = linearLast(transformProperties)
      } else {
        // Choose non-linear easings over linear, choose transforms over non-transforms
        orderedEasings = (nonTransformProperties.length === propertiesWithEasing.length)
          ? linearLast(nonTransformProperties)
          : [...linearLast(transformProperties), ...linearLast(nonTransformProperties)]
      }
      console.warn('Multiple easings for keyframe:', easings, '\n\n', `➡️ Choosing '${easings[orderedEasings[0]]}'`)
      const easing = { 'animation-timing-function': easings[orderedEasings[0]] }

      converted[keyframeTiming] = { easing, ...otherProperties }
    } else if (propertiesWithEasing.length === 0 ) {
      const easing = { 'animation-timing-function': '' }
      converted[keyframeTiming] = { easing, ...otherProperties }
    }
  }
  return converted
}

function makeLoopSafe (keyframesForTarget: KeyframesForTarget): KeyframesForTarget {
  // all animated properties need values declared in the first and last keyframes,
  // otherwise CSS will automatically tween to & from the property's default value at 0% & 100%.
  // This function will repeat a property's last stated value if it isn't declared in the final
  // keyframe, and first stated value if not declared in the first keyframe
  const orderedTimings: number[] = Object.keys(keyframesForTarget).sort((a, b) => parseFloat(a) - parseFloat(b)).map(parseFloat)
  const firstKeyframeTiming = orderedTimings[0]
  const lastKeyframeTiming = orderedTimings.slice(-1)[0]
  const firstKeyframe = keyframesForTarget[firstKeyframeTiming]
  const lastKeyframe = keyframesForTarget[lastKeyframeTiming]
  const isNotInKeyframe = (keyframe: KeyFrame, propName: string) => !getKeyframeProperties(keyframe).includes(propName)
  const allAnimatedProperties = orderedTimings.reduce((acc: string[], timing) => {
    const currentKeyframe = keyframesForTarget[timing]
    acc.push(...getKeyframeProperties(currentKeyframe).filter(prop => !acc.includes(prop)))
    return acc
  }, [])
  const missingProperties = (keyframe: KeyFrame) => Array.from(allAnimatedProperties).filter(propName => isNotInKeyframe(keyframe, propName))

  function addMissingProperties (firstOrLast: string, keyframe: KeyFrame) {
    // returns the keyframe provided, whether updated or not
    if (missingProperties(keyframe).length) {
      return missingProperties(keyframe).reduce((acc: KeyFrame, propName: string) => {
        const keyframeValues = (firstOrLast === 'first')
          ? getKeyframeValues(propName)[0]
          : getKeyframeValues(propName).slice(-1)[0]
        return writeToKeyframe(acc, propName, keyframeValues)
      }, keyframe)
    } else {
      return keyframe
    }
  }
 
  function getKeyframeValues (propName: string) {
    // returns an array of all keyframe values for a property, in order
    return orderedTimings.reduce((acc: string[], timing) => {
      const currentKeyframe = keyframesForTarget[timing]
      const propType = (isTransform(propName)) ? 'transform' : 'property'
      const currentKeyframePropValue = (getKeyframeProperties(currentKeyframe).includes(propName)) ? [currentKeyframe[propType]![propName]] : []
      return [...acc, ...currentKeyframePropValue]
    }, [])
  }

  const updatedKeyframesForTarget = {...keyframesForTarget}

  updatedKeyframesForTarget[firstKeyframeTiming] = addMissingProperties('first', firstKeyframe)
  updatedKeyframesForTarget[lastKeyframeTiming] = addMissingProperties('last', lastKeyframe)

  return updatedKeyframesForTarget
}

function getKeyframeProperties (keyframe: KeyFrame): string[] {
   return [
    ...getTransformProperties(keyframe), 
    ...getNonTransformProperties(keyframe)
  ]
}

function getTransformProperties (keyframe: KeyFrame) {
  return (keyframe.transform) ? Object.keys(keyframe.transform) : []
}

function getNonTransformProperties (keyframe: KeyFrame) {
  return (keyframe.property) ? Object.keys(keyframe.property) : []
}
 
function convertToPercentages (keyframesForTarget: KeyframesForTarget, timelineDuration: number) {
  const absoluteKeyframes = Object.keys(keyframesForTarget).sort((a, b) => { return parseFloat(a) - parseFloat(b) })
  .map(string => parseFloat(string))
  const precision = calculatePrecisionRequired(absoluteKeyframes, timelineDuration)
  const percentageKeyframesAndValues = absoluteKeyframes.reduce((acc: PercentageKeyframesForTarget, absoluteKeyframe: number) => {
    const percentageKeyframe = getPercentageString(absoluteKeyframe, timelineDuration, precision)
    const bookendedKeyframe = addZeroAndHundredPercentStrings(absoluteKeyframe, absoluteKeyframes, percentageKeyframe)
    acc[bookendedKeyframe] = keyframesForTarget[absoluteKeyframe]
    return acc
  }, { })
  return percentageKeyframesAndValues
}

function calculatePrecisionRequired (keyframes: number[], duration: number) {
  const smallestDifference = keyframes.reduce((acc, keyframe, index, arr) =>
    (keyframe - arr[index - 1] < acc || acc === 0) ? keyframe - arr[index - 1] : acc)
  const precision = duration / smallestDifference
  const precisionLevelRequired = (n: number) => Math.max(100, Math.pow(10, Math.floor(Math.log(n) / Math.LN10 + 0.000000001)) / 10)

  return precisionLevelRequired(precision)
}

function getPercentageString (partial: number, total: number, precisionLevel: number) {
  return Math.round(partial / total * 100 * precisionLevel + Number.EPSILON) / precisionLevel + '%'
}

function addZeroAndHundredPercentStrings (absoluteKeyframe: number, absoluteKeyframes: number[], percentageKeyframe: string) {
  // add a 0% & 100% keyframe to each animation so they start in the
  // correct initial position and hold at the end
  const first = Boolean(absoluteKeyframe === absoluteKeyframes[0])
  const last = Boolean(absoluteKeyframe === absoluteKeyframes.slice(-1)[0])

  if (first && percentageKeyframe !== '0%') return '0%, ' + percentageKeyframe
  if (last && percentageKeyframe !== '100%') return percentageKeyframe + ', 100%'

  return percentageKeyframe
}

function removeRedundantKeyframes (targetKeyframes: PercentageKeyframesForTarget): PercentageKeyframesForTarget {
  // avoid needlessly repeating keyframes where the values don't change.
  // converts: 
  //   '24%': { identical keyframe }, 
  //   '25%': { identical keyframe },
  //   '26%': { identical keyframe }
  // to: 
  //   '24%, 26%': { keyframe }

  const keyframeTimings = Object.keys(targetKeyframes)
  const deDuplicatedKeyframes = keyframeTimings.reduce((acc: Map<string, KeyFrame>, currKey: string, index: number) => {
    // for each keyframe, compare against the previous keyframe. are the (transform, property) values the same? 
    const currKeyframe = targetKeyframes[currKey]
    if (index === 0) {
      acc.set(currKey, currKeyframe) 
      return acc
    }

    const prevKey = Array.from(acc)[acc.size-1][0]
    const prevKeyframe = acc.get(prevKey)
    if (prevKeyframe) {
      if (isKeyframeEquivalent(prevKeyframe, currKeyframe)) {
        // add the key strings for these two keyframes together, and delete the first keyframe.

        // if prevKey is '24%, 25%' and currKey is '26%', then we only want '24%, 26%' in the combined key.
        const firstValuePrevKey = prevKey.split(',')[0]
        const combinedKey: string = `${firstValuePrevKey}, ${currKey}`
        acc.delete(prevKey)
        acc.set(combinedKey, currKeyframe)
      } else {
        acc.set(currKey, currKeyframe)
      }
    }
    return acc
  }, new Map())

  return Object.fromEntries(deDuplicatedKeyframes)
}

function isKeyframeEquivalent (a: KeyFrame, b: KeyFrame) {
  const props: ('transform'|'property')[] = ['transform', 'property']
  return (props.every(prop => keysMatch(a, b, prop)))
         ? props.every(prop => isRecordEquivalent(a[prop], b[prop])) ? true : false
         : false
}

function keysMatch (a: KeyFrame, b: KeyFrame, property: 'transform' | 'property') {
  if ((a[property] === undefined) && (b[property] !== undefined)) {
    return false
  } else if ((a[property] !== undefined) && (b[property] === undefined)) {
    return false
  }
  return true
}

function isRecordEquivalent (a: Record<string, string> | undefined, b: Record<string, string> | undefined) {
  const aKeys = (a) ? Object.keys(a) : []
  const bKeys = (b) ? Object.keys(b) : []
  if ((!a && !b) || (!aKeys.length && !bKeys.length)) {
    return true
  } else {
    if (aKeys.length !== bKeys.length) {
      return false
    }
    return Boolean(aKeys.every(property => a![property] === b![property]))
  }
}

function separateConflictingTransforms (animations: TimelineData) {
  const separatedAnimations: TimelineData = { }
  // get the combinations of transforms used in each keyframe 
  for (const target in animations) {
    const transformCombinations: Set<string> = new Set()
    for (const keyframe in animations[target]) {
      const currentKeyframe = animations[target][keyframe]
      if (currentKeyframe.transform && Object.keys(currentKeyframe.transform).length) {
        transformCombinations.add(Object.keys(currentKeyframe.transform).toString())
      }
    }
    // if all keyframes for a target have the same transform combination, or there are 
    // no transforms for this target, nothing needs to be separated
    if (transformCombinations.size <= 1) {
      separatedAnimations[target] = animations[target]
    } else {
      // split up conflicting transforms by adding new parent groups around elements, 
      // and moving some transforms to the parent. 
      const newAnimations: TimelineData = splitAnimations(target, animations[target], shouldTransformsStayOrGo(transformCombinations))
      for (const target in newAnimations) {
        separatedAnimations[target] = newAnimations[target]
      }
    }
  }
  // return an updated TimelineData object, which also includes any new (parent) targets
  return separatedAnimations
}

function arrayEquals(a: string[], b: string[]) {
  return a.length === b.length && a.every((val: string | number, index: number) => val === b[index])
}

function shouldTransformsStayOrGo (transformCombinations: Set<string>): SplitTransforms {
  const combinationsArray: string[][] = Array.from(transformCombinations).map(string => string.split(',')) 
  const allTransformsUsed: Set<string> = new Set([...combinationsArray.flat()])
  const appearsInEveryKeyframe: string[] = Array.from(allTransformsUsed).filter(transform => {
    return combinationsArray.every(combo => combo.includes(transform))
  })
  const onlyInSomeKeyframes: string[] = Array.from(allTransformsUsed).filter(transform => (!appearsInEveryKeyframe.includes(transform)))

  // determine which transforms will stay on the element and which will be moved to a new parent group
  if (onlyInSomeKeyframes.length === 1) {
    return { stay: appearsInEveryKeyframe, go: [onlyInSomeKeyframes] }
  } else {
    // check if any of the 'to-go' transforms share exactly the same keyframes, maybe the transforms can be split off together?

    // get the list of specific transform combinations that each transform appears in
    const combosPerTransform: Record<string, string[]> = {}
    onlyInSomeKeyframes.forEach(transform => {
      combosPerTransform[transform] = Array.from(transformCombinations).filter(combo => combo.includes(transform))
    })
    // check which lists of combinations are unique
    const uniqueCombosPerTransform = Array.from(new Set([...Object.values(combosPerTransform).map(combo => combo.join('+'))]))
    // if they're all unique, they all get split off separately
    if (uniqueCombosPerTransform.length === onlyInSomeKeyframes.length && appearsInEveryKeyframe.length > 0) { 
      return { stay: appearsInEveryKeyframe, go: onlyInSomeKeyframes.map(transform => [transform]) }
    }
    
    const splittingGroups = uniqueCombosPerTransform.reduce((acc: string[][],combo: string) => {
      // group transforms which share the same transform combinations
      acc.push(onlyInSomeKeyframes.filter(transform => arrayEquals(combosPerTransform[transform], combo.split('+'))))
      return acc
    }, [])

    if (appearsInEveryKeyframe.length === 0){
      // at this stage if none of the transforms are in every keyframe, 'stay' will be empty. 
      // Keep the longest array in the list, or the _first_ longest otherwise.
        const maxLength = splittingGroups.reduce((acc, current) => (acc > current.length) ? acc : current.length, 0)
        
        const stay = splittingGroups.filter(transformGroup => transformGroup.length === maxLength)[0]
        const go = splittingGroups.filter(transformGroup => !arrayEquals(stay, transformGroup))

        return { stay: stay, go: go }
    } else {
      return { stay: appearsInEveryKeyframe, go: splittingGroups }
    }
  }
}

function splitAnimations (selector: string, keyframes: KeyframesForTarget, transformGroups: SplitTransforms) {
  // conflicting transform properties will be split, and the keyframes for one transform 
  // (or group of transforms) will be moved to a new wrapper group around the original element
  const newAnimations: TimelineData = { }
  const { stay, go } = transformGroups
  
  moveTransformsToWrapper(keyframes, stay, go)
  
  function moveTransformsToWrapper (keyframes: KeyframesForTarget, stay: string[], go: string[][]) {
    // updates newAnimations with keyframes for both original and new targets
    const elementKeyframes: KeyframesForTarget = { }
    const wrapperKeyframes: KeyframesForTarget = { }

    for (const keyframeTiming in keyframes) {
      const keyframe = keyframes[keyframeTiming]
      const hasTransformProperty = Boolean(getTransformProperties(keyframe).length)
      const isConflicting = includesTransform(keyframe, stay) && includesTransform(keyframe, go)
      const isStaying = (transform: string) => stay.includes(transform)
      const isGoing = (transform: string) => go.flat().includes(transform)
     
      if (!hasTransformProperty) {
        // if there are no transforms, just add the keyframe to the original element's object
        elementKeyframes[keyframeTiming] = keyframe
      } else if (hasTransformProperty && !isConflicting) {
        // add the keyframe to the element or wrapper (depending on the transform).
        (includesTransform(keyframe, stay))
          ? elementKeyframes[keyframeTiming] = keyframe
          : wrapperKeyframes[keyframeTiming] = keyframe
      } else if (isConflicting) {
        // keep the 'stay' transforms and non-transform props on the original element keyframes.
        // move the 'go' transforms to the wrapper keyframes
        for (const transform in keyframe.transform) {
          const value = keyframe.transform[transform]
          const easing = (keyframe.easing[transform]) ? keyframe.easing[transform] : ''
          if (isStaying(transform)) {
            elementKeyframes[keyframeTiming] = writeToKeyframe({}, transform, value, easing)
          } else if (isGoing(transform)) {
            wrapperKeyframes[keyframeTiming] = writeToKeyframe({}, transform, value, easing)
          }
        }
        // keep non-transform properties on the original element
        elementKeyframes[keyframeTiming] = getNonTransformProperties(keyframe).reduce((acc: KeyFrame, propName: string) => {
          const value = keyframe.property![propName]
          const easing = (keyframe.easing[propName]) ? keyframe.easing[propName] : ''
          
          return writeToKeyframe(acc, propName, value, easing)
        }, elementKeyframes[keyframeTiming])
      }
    }

    newAnimations[selector] = elementKeyframes
    
    const wrapperTransformName: string = toKebabCase(go[0].join('-'))
    const wrapperId = (document.querySelector(`svg ${selector}-${wrapperTransformName}`))
      ? `${selector}-${wrapperTransformName}`
      : createWrappingGroup(selector, wrapperTransformName)
    if (wrapperId === undefined) throw new Error("Wrapping Group has no id, or was not created")

    if (go.length === 1) {
      newAnimations[wrapperId] = wrapperKeyframes
    } else {
      // if there are multiple transforms or transform groups that need to be split, 
      // the splitting process will need to be run again.
      // the first array in the existing 'go' group will become the 'stay' transforms for the next loop.
      const [staying, ...rest] = go
      const transformsToSplitAgain: SplitTransforms = {
        stay: staying,
        go: rest
      }
      splitAnimations(wrapperId, wrapperKeyframes, transformsToSplitAgain)
    }
  }

  return newAnimations
}

function includesTransform (keyframe: KeyFrame, transformArray: string[] | string[][]) {
  if (keyframe.transform && Object.keys(keyframe.transform).length) {
    const transformList = Object.keys(keyframe.transform)
    const isInKeyframe = (transform: string) => transformList.includes(transform)
    return transformArray.flat().some(isInKeyframe)
  } else {
    return false
  }
}

function createWrappingGroup (selector: string, transform: string) {
  const currentElement = document.querySelector(`svg ${selector}`) as SVGElement
  const nextSibling = currentElement.nextElementSibling
  const parentGroup = document.createElementNS('http://www.w3.org/2000/svg', 'g')
  const parentGroupId = `${selector}-${transform}`
  parentGroup.setAttribute('id', parentGroupId.replace('#',''))
  const origin = window.getComputedStyle(currentElement).transformOrigin
  parentGroup.style.transformOrigin = origin
  if (currentElement.parentNode !== null) {
    (nextSibling === null)
      ? currentElement.parentNode.appendChild(parentGroup)
      : currentElement.parentNode.insertBefore(parentGroup, nextSibling)
    parentGroup.appendChild(currentElement)
  }
  const parentElement = document.querySelector(`svg ${parentGroupId}`) as SVGElement
  if (parentElement.querySelector(`${selector}`)) {
    return parentGroupId
  } else {
      throw new Error(`Error: ${selector} is not a child of parent #${parentGroupId}`)
  }
}

function getMostCommonEasing(targetKeyframes: KeyframesForTarget) {
  const allEasings: Record<string, number> = { }
  for (const keyframe in targetKeyframes) {
    const thisKeyframeEasing = targetKeyframes[keyframe].easing['animation-timing-function']
    if (thisKeyframeEasing && thisKeyframeEasing.length) {
      allEasings[thisKeyframeEasing] = allEasings[thisKeyframeEasing] + 1 || 1
    }
  }
  return mostCommon(allEasings)[0] // it might be the first-equal most common, that's ok.
}

function mostCommon (obj: Record<string, number>) {
  return Object.keys(obj).filter(key => (obj[key] === Math.max(...Object.values(obj))))
}

function combineTargetsAndDuration (targets: string[], timelineDuration: number) {
  const selectorList = `\n ${targets.join(', ')}`
  const durationInSeconds = Math.round((timelineDuration) / 10 + Number.EPSILON) / 100
  const durationIterationDeclaration = ' {\n  animation-duration: ' + durationInSeconds + 's;\n  animation-iteration-count: infinite;\n}\n'

  return selectorList + durationIterationDeclaration
}

function makeTargetListForEachEasing (defaultEasings: Record<string, string>) {
  const targetsUsingEasing = listTargetIdsUsingEasing(defaultEasings)
  const timingFunctionDeclaration = formatAsTimingFunctionDeclaration(targetsUsingEasing)

  return timingFunctionDeclaration
}

function listTargetIdsUsingEasing (defaultEasings: Record<string, string>) {
  const easingsUsed = new Set(Object.values(defaultEasings))
  const targetsUsingEasing: Record<string, string[]> = { }
  easingsUsed.forEach(easing => {
    targetsUsingEasing[easing] = []
    const getTargets = (defaultEasings: Record<string, string>, easing: string) => Object.keys(defaultEasings).filter(key => defaultEasings[key] === easing)

    targetsUsingEasing[easing].push(...getTargets(defaultEasings, easing))
  })

  return targetsUsingEasing
}

function formatAsTimingFunctionDeclaration (targetsUsingEasing: Record<string, string[]>) {
  const timingFunctionDeclaration = []
  for (const easing in targetsUsingEasing) {
    const targetList = targetsUsingEasing[easing].join().replace(/,/g, ', ')
    timingFunctionDeclaration.push(`\n${targetList} {\n animation-timing-function: ${easing};\n}\n`)
  }
  return timingFunctionDeclaration.join('')
}

function formatKeyframesAsCss (timelineData: PercentageTimelineData, defaultEasings: Record<string, string>) {
  let css = ''
  for (const target in timelineData) {
    css += `\n${target} { animation-name: ${target.replace(/[^a-z,A-Z,-]/g, '')}-anim; }\n@keyframes ${target.replace(/[^a-z,A-Z,-]/g, '')}-anim {\n`
    const keyframesForTarget = timelineData[target]
    for (const keyframeTiming in keyframesForTarget) {
      css += `  ${keyframeTiming} {`
      const keyframe = keyframesForTarget[keyframeTiming]

      if (keyframe.transform && Object.keys(keyframe.transform).length) {
        css += ' transform:'
        for (const transformProp in keyframe.transform) {
          css += ` ${transformProp}(${keyframe.transform[transformProp]})`
        }
        css += ';'
      }

      if (keyframe.property && Object.keys(keyframe.property).length) {
        for (const cssProperty in keyframe.property) {
          css += ` ${toKebabCase(cssProperty)}: ${keyframe.property[cssProperty]};`
        }
      }
      
      if (Object.keys(keyframe.easing).length) {
        const easing = ('animation-timing-function' in keyframe.easing)
          ? keyframe.easing['animation-timing-function']
          : ''
        if (typeof easing === 'string' && easing.length && easing !== defaultEasings[target]) {
          css += ` animation-timing-function: ${easing};`
        } else if (typeof easing !== 'string') { 
          throw new Error(`easing is not a string! ${easing}`)
        }
      }
      css += ' }\n'
    }
    css += '}\n'
  }
  css = ` ${css} /* `
  return css
}

function toKebabCase (string: string) {
  return string.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()
}

function appendCssToDocument (css: string) {
  const originalStyle = document.getElementsByTagName('style')[0]
  const newStyle = document.createElementNS('http://www.w3.org/2000/svg', 'style')
  newStyle.append('\n/* ')
  newStyle.append(document.createCDATASection(css))
  newStyle.append(' */\n')

  !!originalStyle
    ? originalStyle.parentNode!.insertBefore(newStyle, originalStyle.nextSibling)
    : document.documentElement.append(newStyle)
  
  const newline = document.createTextNode('\u000A')
  newStyle.parentNode!.insertBefore(newline, newStyle)
}

function deleteAllScripts () {
  console.log('🎉️ Converted! You can now save your CSS animated SVG.\n(Right-click -> "Save Page As")')
  const scripts = document.getElementsByTagName('script')
  while (scripts.length) scripts[0].remove()
}


interface TweenValue {
  original: string;
  numbers: number[];
  strings: string[];
}

interface Tween {
  value: number[];
  delay: number;
  endDelay: number;
  duration: number;
  easing: (t: number) => number;
  round: number;
  from: TweenValue;
  to: TweenValue;
  start: number;
  end: number;
  isPath: boolean;
  isColor: boolean;
}

interface Animatable {
  target: SVGElement;
}

interface Transforms {
  list: Record<string, string>;
  last: string;
}

interface Animation {
  type: string;
  property: string;
  animatable: Animatable;
  transforms: Transforms;
  tweens: Tween[];
  duration: number;
  delay: number;
  endDelay: number;
  currentValue: string;
}

interface Anime {
  animations: Animation[];
  duration: number;
  delay: number;
  endDelay: number;
  timelineOffset: number;
  children: Anime[];
}

interface TimelineData {
  /** key is the element id */
  [selector: string]: KeyframesForTarget;
}

interface PercentageTimelineData {
  /** key is the element id */
  [selector: string]: PercentageKeyframesForTarget;
}

interface KeyframesForTarget {
  [time: number]: KeyFrame;
}

interface PercentageKeyframesForTarget {
  [percentage: string]: KeyFrame;
}

interface KeyFrame {
  easing: Record<string, string>;
  transform?: Record<string, string>;
  property?: Record<string, string>;
}

interface TweenKeyframe {
  startKeyframe: KeyFrame;
  endKeyframe: KeyFrame;
}

interface Frequencies {
  [key: string]: number;
}

interface SplitTransforms {
  stay: string[],
  go: string[][]
}

interface KeyframesAndDuration {
  timelineData: TimelineData;
  timelineDuration: number;
}